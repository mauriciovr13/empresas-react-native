import React from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    Dimensions, 
    Image,
    TouchableOpacity,
 } from 'react-native';

const EnterpriseCard = ({ enterprise, isFirstColumn, onPress }) => (
    <TouchableOpacity 
        onPress={onPress}
        style={[
            styles.container,
            isFirstColumn ? styles.firstColumn : styles.lastColumn
        ]}>
        <View style={styles.card}>
            <Image
                source={{
                    uri: 'http://empresas.ioasys.com.br' + enterprise.photo
                }}
                aspectRatio={1}
                resizeMode="cover"
            />
            <View style={styles.cardTitleWraper}>
                <Text style={styles.cardTitle}>{enterprise.enterprise_name}</Text>
            </View>
        </View>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        width: '50%',
        padding: 5,
        height: Dimensions.get('window').width/2,
    },
    card: {
        flex: 1,
        borderWidth: 1
    },
    cardTitleWraper: {
        backgroundColor: 'black',
        height: 50,
        
        position: 'absolute',
        bottom: 0,
        opacity: .8,
        width: '100%',
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 3,
        paddingRight: 3,
        alignItems: 'center'
    },
    cardTitle: {
        color: 'white',
        fontSize: 15,
        fontWeight: 'bold',

    },
    firstColumn: {
        paddingLeft: 10
    },
    lastColumn: {
        paddingRight: 10
    }
});

export default EnterpriseCard;