import React from 'react';
import { 
    Text, 
    View, 
    StyleSheet,     
} from 'react-native';


const LongText = ({ label, content }) => {
    return (
        <View style={styles.line}>
            <Text style={[
                styles.cell, 
                styles.label,
                ]}>{ label }</Text>
            <View>
                <Text style={[
                    styles.cell, 
                    styles.content,
                    styles.expanded
                ]}>{ content }</Text>
            </View>            
        </View>
    )
}

const styles = StyleSheet.create({
    line: {
        paddingTop: 3,
        paddingBottom: 3,
    },
    cell: {
        fontSize: 18,
        paddingRight: 5,
        paddingLeft: 5,
    },
    label: {
        fontWeight: 'bold',
        flex: 1,
        textDecorationLine: 'underline',
        paddingBottom: 8
    },
    content: {
        flex: 3,
        textAlign: 'justify' //IOS
    },    
    expanded: {
        flex: 1
    }
});

export default LongText;