import React from 'react';
import { View, Button, StyleSheet, Picker } from 'react-native';
import { connect } from 'react-redux';
import Input from './Input';
import { setName, setType, filterEnterprises, watchEnterprises } from '../actions';

class SearchBox extends React.Component {   

    onChangeText(text) {
        this.props.dispatchSetName(text);        
    }

    onChangeType(type) {        
        this.props.dispatchSetType(type)
    }

    onPress() {
        this.props.filterEnterprises(this.props)
    }
    
    onClean() {
        this.props.watchEnterprises(this.props);
        this.props.dispatchSetType('')
        this.props.dispatchSetName('')

    }

    render() {
        const { enterprise_type, name } = this.props
        return (
            <View>
                <View style={styles.formContainer}>
                    <View style={styles.inputContainer}>
                        <Input
                            onChangeText={text => this.onChangeText(text)}                            
                            value={ name }
                            />
                    </View>
                        
                    <Picker 
                        selectedValue={ enterprise_type ? `${enterprise_type}` : '0'}
                        onValueChange={(itemValue, itemIndex) => this.onChangeType(itemValue)}
                        style={styles.pickerContainer}
                    >
                        <Picker.Item label="Selecione um tipo" value="0" />
                        <Picker.Item label="Agro" value="1" />
                        <Picker.Item label="Aviation" value="2" />
                        <Picker.Item label="Biotech" value="3" />
                        <Picker.Item label="Eco" value="4" />
                        <Picker.Item label="Ecommerce" value="5" />
                        <Picker.Item label="Education" value="6" />
                        <Picker.Item label="Fashion" value="7" />
                        <Picker.Item label="Fintech" value="8" />
                        <Picker.Item label="Food" value="9" />
                        <Picker.Item label="Games" value="10" />
                        <Picker.Item label="Health" value="11" />
                        <Picker.Item label="IOT" value="12" />
                        <Picker.Item label="Logistics" value="13" />
                        <Picker.Item label="Media" value="14" />
                        <Picker.Item label="Mining" value="15" />
                        <Picker.Item label="Products" value="16" />
                        <Picker.Item label="Real Estate" value="17" />
                        <Picker.Item label="Service" value="18" />
                        <Picker.Item label="Smart City" value="19" />
                        <Picker.Item label="Social" value="20" />
                        <Picker.Item label="Software" value="21" />
                        <Picker.Item label="Technology" value="22" />
                        <Picker.Item label="Tourism" value="23" />
                        <Picker.Item label="Transport" value="24" />                        
                    </Picker>
                </View>
                <View style={styles.formContainer}>
                    <View style={ styles.buttonContainer }>
                        <Button	
                            title="Filtrar" 
                            onPress={() => this.onPress()}
                            color='#050f0f'
                        />
                    </View>
                    <View style={ styles.buttonContainer }>
                        <Button	
                            title="Limpar"
                            onPress={() => this.onClean()}
                            color='#050f0f'
                        />
                    </View>
                </View>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    formContainer: {
        flexDirection: 'row',
        padding: 5,
	},
	inputContainer: {
		flex: 2
	},
	buttonContainer: {
        flex: 2,
        padding: 5
    },
    pickerContainer: {
        flex: 2
    },
});

const mapStateToProps = state => {
    const { filterEnterprise } = state;
    const { name, enterprise_type } = filterEnterprise;
    const { enterprises, user } = state;
    return { 
                enterprises, 
                user, 
                name, 
                enterprise_type
            };
}

export default connect(mapStateToProps, 
    { 
        dispatchSetName: setName,
        dispatchSetType: setType,
        filterEnterprises,
        watchEnterprises 
    }
)(SearchBox);