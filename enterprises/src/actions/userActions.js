import axios from 'axios';

export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
const userLoginSuccess = user => ({
    type: USER_LOGIN_SUCCESS,
    user
});

export const tryLogin = ({ email, password }) => dispatch => {    
    return axios({
            method: 'POST',
            url: 'http://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
            data: {
                email: email,
                password: password
            }
        }).then(response => {

            const user = {
                'access-token': response.headers["access-token"],
                'client': response.headers["client"],
                'uid': response.headers["uid"]
            }

            const action = userLoginSuccess(user);
            dispatch(action);
            return user;

        }).catch((error) => {
            // console.log(error)
            return Promise.reject(error)
        })
}