import axios from 'axios';

export const SET_ENTERPRISES = 'SET_ENTERPRISES'
const setEnterprises = enterprises => ({
    type: SET_ENTERPRISES,
    enterprises,
});

export const SET_NAME = 'SET_NAME'
export const setName = name => ({
    type: SET_NAME,
    name,
});

export const SET_TYPE = 'SET_TYPE';
export const setType = enterprise_type => ({
    type: SET_TYPE,
    enterprise_type,
});


export const watchEnterprises = ({ user }) => {
    return dispatch => {
        axios({
            method: 'GET',
            url: 'http://empresas.ioasys.com.br/api/v1/enterprises',
            headers: {
                'access-token': user["access-token"],
                'client': user['client'],
                'uid': user['uid']
            }
        }).then((response => {
            // Tratando a response da API
            const enterprises = response.data.enterprises;
            const action = setEnterprises(enterprises);
            dispatch(action);
        }))
        .catch(error => {
            console.log("Deu erro");
        })
    }
}

export const filterEnterprises =({ name, enterprise_type, user }) => {
    return dispatch => {
            axios({
            method: 'GET',
            url: 'http://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types='+enterprise_type+'&name='+name,
            headers: {
                'access-token': user["access-token"],
                'client': user['client'],
                'uid': user['uid']
            }
        }).then((response => {
            // Tratando a response da API
            const enterprises = response.data.enterprises;
            const action = setEnterprises(enterprises);
            dispatch(action);
        }))
        .catch(error => {
            console.log("Deu erro");
        })
    }
}