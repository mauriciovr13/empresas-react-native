import { SET_ENTERPRISES } from '../actions';

export default function enterprisesReduction (state = null , action) {
    switch (action.type) {
        case SET_ENTERPRISES:
            return action.enterprises;
        default:
            return state;
    }
}