import { SET_TYPE, SET_NAME } from '../actions';

const INITIAL_STATE = {
    enterprise_type: '',
    name: ''
}

export default function fileterEnterprisesReduction (state = INITIAL_STATE , action) {
    switch (action.type) {        
        case SET_TYPE:
            return {
                    ...state,
                    enterprise_type: action.enterprise_type
            };
        case SET_NAME:
            return {
                    ...state,
                    name: action.name
            };
        default:
            return state;
    }
}