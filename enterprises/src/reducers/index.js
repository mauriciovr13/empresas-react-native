import { combineReducers } from 'redux';

import userReducer from './userReducer';
import enterprisesReducer from './enterprisesReducer';
import filterEnterprisesReducer from './filterEnterprisesReducer';


export default combineReducers({
    user: userReducer,
    enterprises: enterprisesReducer,
    filterEnterprise: filterEnterprisesReducer
})