import { createAppContainer, createStackNavigator } from 'react-navigation';

//Importar as paginas aqui
import LoginPage from './pages/LoginPage';
import EnterprisesPage from './pages/EnterprisesPage';
import EnterpriseDetailPage from './pages/EnterpriseDetailPage';

const AppNavigator = createStackNavigator({
    'Login': {
        screen: LoginPage,
        navigationOptions: {
            title: 'Bem Vindo!'
        }
    },
    'Main': {
        screen: EnterprisesPage,
    },
    'EnterpriseDetail': {
        screen: EnterpriseDetailPage,
        navigationOptions: ({ navigation }) => {
            const { enterprise } = navigation.state.params
            return {
                title: enterprise.enterprise_name
            }
        }
    },    
}, {
    defaultNavigationOptions: {
        title: 'Empresas',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: '#050f0f',
            borderBottomWidth: 1,
            borderBottomColor: '#c5c5c5'
        },
        headerTitleStyle: {
            color: 'white',
            fontSize: 30,

        }
    }
});

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;