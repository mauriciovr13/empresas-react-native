import React from 'react';
import { View, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import EnterpriseCard from '../components/EnterpriseCard';
import SearchBox from '../components/SearchBox';
import { connect } from 'react-redux';
import { watchEnterprises } from '../actions';

// Função para verificar se o componente do flat list será renderizado
// a direita ou esquerda.
const isEven = number => number % 2 === 0;

class EnterprisesPage extends React.Component {

    componentDidMount() {
        this.props.watchEnterprises(this.props);
    }

    render() {    
        const { navigation, enterprises } = this.props;

        if (enterprises === null) {
            return <ActivityIndicator />
        }
        return (
            <View>
                {/* <SearchBox /> */}
                <FlatList
                    data={enterprises}
                    renderItem={({ item, index }) => (
                        <EnterpriseCard 
                            enterprise={item}
                            isFirstColumn={isEven(index)}
                            onPress={ () => navigation.navigate('EnterpriseDetail', { enterprise: item })}
                        />
                    )}
                    keyExtractor={item => item.id}
                    numColumns={2}
                    ListHeaderComponent={props => ( <SearchBox /> )}
                    ListFooterComponent={props => ( <View style={styles.marginBottom} /> )}
                />
            </View>
        )
    }
};

const styles = StyleSheet.create({
    marginTop: {
        marginTop: 5,
    },
    marginBottom: {
        marginBottom: 5,
    }
})

const mapStateToProps = state => {    
    const { enterprises, user } = state;    
    return { 
                enterprises, 
                user 
            }    
}

export default connect(mapStateToProps, { watchEnterprises } )(EnterprisesPage);