import React from 'react';
import { ScrollView, StyleSheet, Image } from 'react-native';

import Line from '../components/Line';
import LongText from '../components/LongText';

class EnterpriseDetailPage extends React.Component {
    render() {
        const { enterprise } = this.props.navigation.state.params;
        return (
            <ScrollView>
                <Image 
                    style={styles.image}
                    source={{
                        uri: 'http://empresas.ioasys.com.br' + enterprise.photo
                    }}
                />
                <Line label='Nome' content={enterprise.enterprise_name} />
                <Line label='Cidade' content={enterprise.city} />
                <Line label='Pais' content={enterprise.country} />
                <Line label='Categoria' content={enterprise.enterprise_type.enterprise_type_name} />
                <Line label='Telefone' content={enterprise.phone} />
                <Line label='Preço ação' content={enterprise.share_price} />
                <Line label='E-mail' content={enterprise.email_enterprise} />
                <LongText label='Descrição' content={enterprise.description} />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        aspectRatio: 1
    }
})

export default EnterpriseDetailPage;