import React from 'react';
import { View, StyleSheet, TextInput, Button, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { tryLogin } from '../actions';

import FormRow from '../components/FormRow';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            isLoading: false,
            message: ''
        }
    }

    onChangeHandler(field, value) {
        this.setState({ [field]: value })
    }

    tryLogin() {    
        this.setState({ isLoading: true, message: '' });
        const { email, password } = this.state;

        this.props.tryLogin({ email, password })
            .then((user) => {
                if(user) {
                    return this.props.navigation.replace('Main');
                }
            }).catch(error =>{        
                this.setState({ isLoading: false })
            })
    }
    
    renderButton() {
        if(this.state.isLoading)
            return <ActivityIndicator />;
        return (
            <Button 
                title="Entrar" 
                onPress={() => this.tryLogin()}
                color='#050f0f'
            />
        )
    }

    render() {
        return (
            <View>
                <FormRow first>
                    <TextInput 
                        style={styles.input}
                        placeholder="email@mail.com"
                        value={this.state.email}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        onChangeText={value => this.onChangeHandler('email', value)}
                    />
                </FormRow>
                <FormRow last>
                    <TextInput 
                        placeholder="*********"
                        secureTextEntry
                        value={this.state.password}
                        onChangeText={value => this.onChangeHandler('password', value)}
                    />
                </FormRow>                    
                { this.renderButton() }
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        paddingLeft: 10,
        paddingRight: 10
    },
    input: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
    }
});

export default connect(null, { tryLogin })(LoginPage);