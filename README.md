![N|Solid](logo_ioasys.png)

# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações necessárias para execução do projeto Empresas.

---

### Bibliotecas utilizadas ###

* `axios` - Biblioteca adicionada para fazer a comunicação com a API, usando os verbos HTTP.
* `react-navigation` - Usada para gerenciar a navegação no app, customizando páginas (definindo titulo e outras configurações) e o container da aplicação.
* `react-redux` - Usada para prover acesso a store do redux para todos os componentes utilizando o Provider e, para fazer a conexão entre o componente e o state do redux.
* `redux` - Usado para criar store do redux e gerenciar middleware adicionados
* `reduxThunk` - Middleware adicionado ao criar a store, utilizado para prover uma execução assincrona em dispatch de actions.
* `remote-redux-devtools` - ferramenta usada no browser para visualizar o state do redux, verificando se os reducers estão funcionando.

### Intruções para execução do projeto

* Para executar o projeto será necessário possuir um aparelho android/IOS ou um emulador de qualquer um dos dois sistemas
* Caso use um aparelho físico, deve-se instalar o app `expo` que emula o app em desenvolvimento no aparelho físico.
* O gerenciador de pacotes usado é o `yarn`.
* Entre na pasta do projeto `./enterprise/`
* Abra o terminal e execute `yarn run android` para emular em android ou `yarn run ios` para emular em IOS.
* Se utilizar um emulador de dispositivo, o app se abrirá automaticamente.
* Caso use o aparelho físico, você precisa ler o QR que irá aparecer no browser com o app `expo` e aguardar carregar todo o conteúdo.
* Lembrando que a máquina e o aparelho físico utilizados devem estar conectados ao mesmo roteador.


### Escopo do Projeto
* O Login e acesso de Usuário já registrados
	* Para o login usamos padrões OAuth 2.0. Na resposta de sucesso do login a api retornará 3 custom headers:
		* `access-token`;
		* `client`;
		* `uid`;
	* Para ter acesso às demais APIs precisamos enviar esses 3 (três) custom headers para a API autorizar a requisição;
* Endpoints disponíveis:
	* Listagem de Empresas: `/enterprises`
	* Detalhamento de Empresas: `/enterprises/{id}`
	* Filtro de Empresas por nome e tipo: `/enterprises?enterprise_types={type}&name={name}`

### Dados para Teste ###
* Servidor: http://empresas.ioasys.com.br
* Versão da API: v1
* Usuário de Teste: testeapple@ioasys.com.br
* Senha de Teste : 12341234
